package uz.Maruf.Webserver;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import uz.Maruf.Webserver.router.MainRouter;
public class MainVerticle extends AbstractVerticle {

    @Override
    public void start() {

        Router router = new MainRouter().getRouter(vertx);
        vertx
                .createHttpServer()
                .requestHandler(router)
                .listen(8080);

    }
}

