package uz.Maruf.Webserver.controller;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine;

public class GeneralController {
    private FreeMarkerTemplateEngine templateEngine;

    public  GeneralController(Vertx vertx){
        this.templateEngine =FreeMarkerTemplateEngine.create(vertx   );
    }

    public void inndexPage(RoutingContext ctx){

        JsonObject data = new JsonObject()
                .put("title","Hello World , I am Title ")
                .put("nameIcons","Ikonka ");
        templateEngine
                .render(data ,"templates/home.ftl",
                        res->{
                            if (res.succeeded()) {
                                ctx.response().end(res.result());
                            } else {
                                ctx.fail(res.cause());
                            }
                        });
    }
}
