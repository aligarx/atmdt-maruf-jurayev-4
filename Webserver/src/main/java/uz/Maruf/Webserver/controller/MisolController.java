package uz.Maruf.Webserver.controller;

import io.vertx.ext.web.RoutingContext;

public class MisolController {
    public void faktor(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        int a1 = Integer.parseInt(son1);
        int s=1;
        for (int i=0;i<=a1;i++){
            s=s*i;
        }

        routingContext.response().end(a1+"faktorial"+s+"ga teng");
    }
    public void yosh(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        int a1 = Integer.parseInt(son1);
        if (a1<16){
            routingContext.response().end("O`smir boladi"+a1);
        }
        if (16<a1&&a1<30){
            routingContext.response().end("Yosh boladi"+a1);
        }
        if (30<a1&&a1<45){
            routingContext.response().end("O'rta yosh boladi"+a1);
        }
        if (45<a1){
            routingContext.response().end("Keksa boladi"+a1);
        }
    }
    public void raqamyigindi(RoutingContext routingContext){
        String son1=routingContext.request().params().get("massiv");
        int a1 = Integer.parseInt(son1);
        int a2,s = 0;
        while (a1!=0){
            a2 = a1%10;
            a1 = a1/10;
            s = s + a2;
        }
        routingContext.response().end(String.valueOf(s));
    }

    public void talaba(RoutingContext  routingContext){
        routingContext.response().end(
                "Abdullayev Arzigul,\n" +
                "Abdullayev Omadyor,\n" +
                "Akbarov Axror,\n" +
                "Botirov Sunnat,\n"+
                "Bozorov Ali,\n" +
                "4-kurs ATMDT," );
    }
    public void ichimlik(RoutingContext routingContext){
        routingContext.response().end(
                "Cola cola,\n "+
                        "Pepsi cola,\n "+
                        "RC cola\n, "+
                        "Fanta,\n "+
                        "Sprite,\n "+
                        "Aqua,\n "+
                        "OK,\n "+
                        "Sitro,\n "+
                        "Tizer"

        );
    }
    public void archa(RoutingContext routingContext){
        routingContext.response().end(
                "    *    \n"+
                        "   ***   \n"+
                        "  *****  \n"+
                        " ******* \n"+
                        "*********\n"+
                        "    **   "
        );
    }
    public void bayroq(RoutingContext routingContext){
        routingContext.response().end(
                "______________________________________________________________\n"+
                        "|    (                                                        |\n"+
                        "|  ((   *   *   *                                             |\n"+
                        "| ((    *   *   *   *                                         |\n"+
                        "|  ((  *   *   *   *    *                                     |\n"+
                        "|    (                                                        |\n"+
                        "|=============================================================|\n"+
                        "|                                                             |\n"+
                        "|                                                             |\n"+
                        "|=============================================================|\n"+
                        "|                                                             |\n"+
                        "|                                                             |\n"+
                        "|                                                             |\n"+
                        "|                                                             |\n"+
                        "|                                                             |\n"+
                        "|_____________________________________________________________|"
        );
    }

}
