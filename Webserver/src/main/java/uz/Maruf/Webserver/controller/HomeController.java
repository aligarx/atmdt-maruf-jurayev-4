package uz.Maruf.Webserver.controller;

import io.vertx.ext.web.RoutingContext;
public class HomeController {
    protected String tableName = "General";

    public void getList(RoutingContext ctx) {
        ctx.response().end(tableName + " :: get List");
    }
    public void getById(RoutingContext ctx) {
        ctx.response().end(tableName + " :: getById");
    }
    public void save(RoutingContext ctx) {
        ctx.response().end(tableName + " :: save");
    }
    public void delete(RoutingContext ctx) {
        ctx.response().end(tableName + " :: delete");
    }

    public void bosh(RoutingContext routingContext) {
        routingContext.response().end("ATMDT 4- Kurs Jo'rayev Ma`rufbek  !!!"
        );
    }

}
