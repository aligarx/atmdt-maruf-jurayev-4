package uz.Maruf.Webserver.router;
import io.vertx.core.Vertx;
import uz.Maruf.Webserver.controller.GeneralController;

public class GeneralRouter extends HomeRouter{

    GeneralController generalController =new GeneralController(this.vertx);
    public GeneralRouter(Vertx vertx) {
        super(vertx);
        mapping();
    }

    @Override
    protected void mapping() {
        getRouter().route().handler(generalController::inndexPage);
    }

}
