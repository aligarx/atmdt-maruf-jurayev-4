package uz.Maruf.Webserver.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import uz.Maruf.Webserver.controller.HomeController;

public class HomeRouter {
    protected Vertx vertx;
    private Router router;
    protected HomeController controller;
    public HomeRouter(Vertx vertx) {
        this.vertx = vertx;
        this.router = Router.router(vertx);
    }

    public Router getRouter() {
        return this.router;
    }
    protected void mapping() {
        router.route("/list").handler(controller::getList);
        router.route("/byId").handler(controller::getById);
        router.route("/save").handler(controller::save);
        router.route("/delete").handler(controller::delete);
    }
}
