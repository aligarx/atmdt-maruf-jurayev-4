package uz.Maruf.Webserver.router;

import io.vertx.core.Vertx;
import uz.Maruf.Webserver.controller.AboutController;
import uz.Maruf.Webserver.controller.ContactController;

public class ContactRouter extends HomeRouter {
    ContactController contactController = new ContactController(this.vertx);

    public ContactRouter(Vertx vertx) {
        super(vertx);
        mapping();
    }
    @Override
    protected void mapping() {
        getRouter().route().handler(contactController::inndexPage);
    }
}
