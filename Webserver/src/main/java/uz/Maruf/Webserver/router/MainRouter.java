package uz.Maruf.Webserver.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import uz.Maruf.Webserver.controller.MainController;
import uz.Maruf.Webserver.controller.MisolController;


public class MainRouter {
    private MainController mainController;
    private MisolController  misolController;
    public MainRouter(){

        mainController = new MainController();
        misolController = new MisolController();
    }

    public Router getRouter(Vertx vertx){

        Router router = Router.router(vertx);

        router.route("/assets/*").handler(StaticHandler.create()
        .setCachingEnabled(true)
        .setWebRoot("assets"));

        router.route().handler(BodyHandler.create());
        router.route("/yunalish").handler(mainController::kurs);
        router.route("/stipendiya").handler(mainController::stipendiya);
        router.route("/yigindi/:son1/:son2").handler(mainController::yigindi);
        router.route("/ayirma/:son1/:son2").handler(mainController::ayirma);
        router.route("/kopaytma/:son1/:son2").handler(mainController::kopaytma);
        router.route("/bolish/:son1/:son2").handler(mainController::bolish);
        router.route("/conventor/:son1").handler(mainController::conventor);
        router.route("/sm/:son1").handler(mainController::metr);
        router.route("/ildiz/:son1").handler(mainController::ildiz);
        router.route("/faktorial/:son1").handler(misolController::faktor);
        router.route("/yosh/:son1").handler(misolController::yosh);
        router.route("/raqamyigindi/:massiv").handler(misolController::raqamyigindi);
        router.route("/talaba").handler(misolController::talaba);
        router.route("/ichimlik").handler(misolController::ichimlik);
        router.route("/archa").handler(misolController::archa);
        router.route("/bayroq").handler(misolController::bayroq);

        router.mountSubRouter("/gallery", new GalleryRouter(vertx).getRouter());
        router.mountSubRouter("/about", new AboutRouter(vertx).getRouter());
        router.mountSubRouter("/home", new GeneralRouter(vertx).getRouter());
        router.mountSubRouter("/contact",new ContactRouter(vertx).getRouter());
        router.route().handler(this::page404);
        return router;
    }

    public void page404(RoutingContext  routingContext){

        routingContext
                .response()
                .setStatusCode(404)
                .end("xato 404");
    }
}
