package uz.Maruf.Webserver.router;
import io.vertx.core.Vertx;
import uz.Maruf.Webserver.controller.AboutController;

public class AboutRouter extends HomeRouter{
    AboutController controller =new AboutController(this.vertx);

    public AboutRouter(Vertx vertx) {
        super(vertx);
        mapping();
    }
    @Override
    protected void mapping() {
        getRouter().route().handler(controller::inndexPage);
    }
}
