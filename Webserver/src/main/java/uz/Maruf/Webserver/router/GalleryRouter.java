package uz.Maruf.Webserver.router;
import io.vertx.core.Vertx;
import uz.Maruf.Webserver.controller.GalleryController;

public class GalleryRouter extends HomeRouter {
    GalleryController galleryController =new GalleryController(this.vertx);

    public GalleryRouter(Vertx vertx) {
        super(vertx);
        mapping();
    }
    @Override
    protected void mapping() {
        getRouter().route().handler(galleryController::inndexPage);
    }
}
