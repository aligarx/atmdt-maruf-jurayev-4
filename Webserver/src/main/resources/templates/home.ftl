<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Uzfarm.uz</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
   	<link rel="stylesheet" href="/assets/css/main.css">
    <link href="/assets/css/custom.css" rel="stylesheet">
	<link rel="stylesheet" href="/assets/css/icomoon-social.css">
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css">
	<script src="/assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.ftl"><a href="home.ftl" style="font-size: 50px;">Uzfram.uz</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="home.ftl">Bosh sahifa</a></li>
                    <li><a href="about.ftl">Biz haqimizda</a></li>
                    <li><a href="gallery.ftl">Galeriya</a></li>
                    <li><a href="contact.ftl">Aloqa</a></li>
                </ul>
            </div>
        </div>
    </header>
    <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active" style="background-image: url(/assets/img/slides/4.jpg)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content centered">
                                    <h2 class="animation animated-item-1">Uzfarm fermer xo'jaligi</h2>
                                    <p class="animation animated-item-2">Biz doim siz haqingizda qayg'uramiz. Davlatimizning bizga yaratib bergan imkoniyatliridan oqilona foydalanaylik. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item" style="background-image: url(/assets/img/slides/5.jpg)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered">
                                    <h2 class="animation animated-item-1">Dehqon boy bo'lsa xalq boy bo'ladi</h2>
                                    <p class="animation animated-item-2">Kuzgi shudgor bahorgi ishlarni belgilab beradi. </p>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                <div class="item" style="background-image: url(/assets/img/slides/6.jpg)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content centered">
                                    <h2 class="animation animated-item-1">Serquyosh o'lkam</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="icon-angle-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="icon-angle-right"></i>
        </a>
    </section>
		
	    <div class="section section-dark">
			<div class="container">
				<div class="row">
					<div class="col-md-12">	</div>
				</div>
			</div>
		</div>
        <div class="section section-white">
	        <div class="container">
	        	<div class="row">
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-home"></i>
		        			<h3>Bizning manzil</h3>
		        			<p>Surxondaryo viloyati Qumqo'g'on tummani "Surxon sohili" fermer xo'jaligi. Biz doim siz haqingizda qayg'uramiz. Biz bilan bog'laning.</p>
		        			<a href="#" class="btn">O'qish</a>
		        		</div>
	        		</div>
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-briefcase"></i>
		        			<h3>Shartnomalar</h3>
		        			<p>Shartnoma bo'yicha yillik reja va planlarni o'z vaqtida bajarish. Yangi texnologiyalar va zamonaviy uskunalarni ish tadbiq qilish.</p>
		        			<a href="#" class="btn">O'qish</a>
		        		</div>
	        		</div>
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-calendar"></i>
		        			<h3>Rejalar ro'yxati</h3>
		        			<p>Asalarichilik xo'jaligini  tashkil etib bu sohada faoliyat ko'satayotgan olimlardan tajriba almashish va ularni ish jarayoniga taklif etish.</p>
		        			<a href="#" class="btn">O'qish</a>
		        		</div>
	        		</div>
	        	</div>
	        </div>
	    </div>
<hr>
        <div class="section section-white">
	        <div class="container">
	        	<div class="row">
					<div class="section-title">
				<h1>Bizning ishlar</h1>
				</div>

			<ul class="grid cs-style-3">
	        	<div class="col-md-4 col-sm-6">
					<figure>
						<img src="/assets/img/portfolio/4.jpg" alt="img04" width="1200" height="250">
						<figcaption>
							<h3>Zotdor mollar</h3>
							<span>Golland nasli</span>
							
						</figcaption>
					</figure>
	        	</div>	
				<div class="col-md-4 col-sm-6">
					<figure>
						<img src="/assets/img/portfolio/1.jpg" alt="img01"width="1200" height="250">
						<figcaption>
							<h3>So'ngi texnikalar</h3>
							<span>Amerika traktorlari</span>
							
						</figcaption>
					</figure>
				</div>
				<div class="col-md-4 col-sm-6">
					<figure>
						<img src="/assets/img/portfolio/2.jpg" alt="img02"width="1200" height="250">
						<figcaption>
							<h3>Eski texnikalar</h3>
							<span>Belarus traktorlari</span>
							
						</figcaption>
					</figure>
				</div>
				<div class="col-md-4 col-sm-6">
					<figure>
						<img src="/assets/img/portfolio/5.jpg" alt="img05"width="1200" height="250">
						<figcaption>
							<h3>Zotdor mollar</h3>
							<span>Polsha buqalari</span>
						</figcaption>
					</figure>
				</div>
				<div class="col-md-4 col-sm-6">
					<figure>
						<img src="/assets/img/portfolio/3.jpg" alt="img03"width="1200" height="250">
						<figcaption>
							<h3>O'zimizning texnikalar</h3>
							<span>TZZ traktorlari</span>
						</figcaption>
					</figure>
				</div>
				<div class="col-md-4 col-sm-6">
					<figure>
						<img src="/assets/img/portfolio/6.jpg" alt="img06"width="1200" height="250">
						<figcaption>
							<h3>Bizning ferma</h3>
							<span>Surxon sohili</span>
						</figcaption>
					</figure>
				</div>
			</ul>
	        	</div>
	        </div>
	    </div>			
<hr>

	    <div class="footer">
	    	<div class="container">
			
		    	<div class="row">
				
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Aloqa</h3>
		    			<p class="contact-us-details">
	        				<b>Manzil:</b> Surxondaryo viloyati Qumqo'g'on tumani Guliston mahallasi 115 uy<br/>
	        				<b>Telefon:</b> +99891 191 91 95<br/>
	        				<b>Fax:</b> +1 123 45678910<br/>
	        				<b>Email:</b> <a href="#">surxonsohili@gmail.com</a>
	        			</p>
		    		</div>				
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Reklama</h3>
						<p>Dashnobod anorlari yana qayta yetishtirila boshlandi Ular o'z dovrig'ini yana qayta qozonadi. Bu yo'lda ishlar boshlab yuborildi.</p>
		    			<div>
		    				<img src="/assets/img/icons/facebook.png" width="32" alt="Facebook">
		    				<img src="/assets/img/icons/twitter.png" width="32" alt="Twitter">
		    				<img src="/assets/img/icons/linkedin.png" width="32" alt="LinkedIn">
							<img src="/assets/img/icons/rss.png" width="32" alt="RSS Feed">
						</div>
		    		</div>
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Fermerlar haqida</h3>
		    				<p>Agar siz ham fermer bo'lishini xoxlasangiz va sizda innovatsion g'oyalar bo'lsa o'zingiz yashab turgan tuman hokimligiga uchrashing va ro'yxatdan o'ting. Murojat uchun telefon +99891 191 91 95</p>
		    		</div>

		    	</div>
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="footer-copyright">&copy; 2019 <a href="#">Uzfarm.uz</a> Xizmatlar litsenziyalangan.</div>
		    		</div>
		    	</div>
		    </div>
	    </div>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/assets/js/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="/assets/js/bootstrap.min.js"></script>
		
		<!-- Scrolling Nav JavaScript -->
		<script src="/assets/js/jquery.easing.min.js"></script>
		<script src="/assets/js/scrolling-nav.js"></script>		

    </body>
</html>