<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Uzfarm.uz</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
   	<link rel="stylesheet" href="/assets/css/main.css">
    <link href="/assets/css/custom.css" rel="stylesheet">
	<script src="//use.edgefonts.net/bebas-neue.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/assets/css/icomoon-social.css">
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css">
	<script src="/assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.ftl"><a href="home.ftl" style="font-size: 50px;">Uzfram.uz</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="home.ftl">Bosh sahifa</a></li>
                    <li class="active"><a href="about.ftl">Biz haqimizda</a></li>
                    <li><a href="gallery.ftl">Galeriya</a></li>
                    <li><a href="contact.ftl">Aloqa</a></li>
                </ul>
            </div>
        </div>
    </header>
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Biz haqimizda</h1>
					</div>
				</div>
			</div>
		</div>
        
        <div class="section">
	    	<div class="container">
				<div class="row">
				<div class="col-sm-4">
				<img class="img-responsive" src="/assets/img/ferma.jpg" alt="About Us">
				</div>
				<div class="col-sm-8">
						<h2>Surxon sohili</h2>
						<h3>Tarixi</h3>
						<p>
							Fermer xo'jaligiga 2017 yil asos solingan. Prezidentimizning yoshlar yaratib bergan imkoniyatlardan to'g'ri foydalangan holda asos solingan edi. Hozir fermer xo'jaligi chorvachilik, g'allachilik va paxtachilik bilan shug'ullanadi.
						</p>
						<p>
							"Surxon sohili" fermer xo'jaligi hozirgi vaqtga kelib 200 ming gektar yer maydoniga va 500 bosh zotdor qora mollarga ege. Keyingi yildan boshlab viloyat uchun go'sht mahsulotlarining asosiy qismini yetkazib berishni o'z oldiga maqsad qilib olgan.
						</p>						
						<h3>Asalarichilik</h3>
						<p>
							Asalarichilikni rivojlantirishni o'z oldiga maqsad qilgan bizning ahil jamoa hozir o'z ishchilarini malakasini oshirish uchun ko'plab o'quv kurslarini tashkil etib bermoqda. Bu bilan cheklanib qolmasdan xorijdan ham mutaxasislarni jalb qilmoqchi va eng so'ngi texnologiyalardan foydalanish rejalarini imkoniyatlarini ko'rib chiqayapti.
						</p>
							
					</div>
				</div>
			</div>
		</div>
<hr>		

        <div class="section">
	    	<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="team-member">
							<div class="team-member-image"><img src="/assets/img/team/1.jpg" alt="Name Surname"></div>
							<div class="team-member-info">
								<ul>
									<li class="team-member-name">
										Asrlon Qodirov
										<span class="team-member-social">
											<a href="#"><i class="icon-facebook"></i></a>
											<a href="#"><i class="icon-github"></i></a>
											<a href="#"><i class="icon-tumblr"></i></a>
										</span>
									</li>
									<li>Bosh hisobchi</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Team Member -->
					<div class="col-md-4 col-sm-6">
						<div class="team-member">
							<div class="team-member-image"><img src="/assets/img/team/2.jpg" alt="Name Surname"></div>
							<div class="team-member-info">
								<ul>
									<li class="team-member-name">
										Ergash Jumayev
										<span class="team-member-social">
											<a href="#"><i class="icon-facebook"></i></a>
											<a href="#"><i class="icon-dribbble"></i></a>
											<a href="#"><i class="icon-tumblr"></i></a>
										</span>
									</li>
									<li>Fermer xo'jaligi boshlig'i</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="team-member">
							<div class="team-member-image"><img src="/assets/img/team/3.jpg" alt="Name Surname"></div>
							<div class="team-member-info">
								<ul>
									<li class="team-member-name">
										Fotima Zokirova
										<span class="team-member-social">
											<a href="#"><i class="icon-facebook"></i></a>
											<a href="#"><i class="icon-dribbble"></i></a>
											<a href="#"><i class="icon-tumblr"></i></a>
										</span>
									</li>
									<li>Ferma yuristi</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<hr>

	     <div class="footer">
	    	<div class="container">
			
		    	<div class="row">
				
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Aloqa</h3>
		    			<p class="contact-us-details">
	        				<b>Manzil:</b> Surxondaryo viloyati Qumqo'g'on tumani Guliston mahallasi 115 uy<br/>
	        				<b>Telefon:</b> +99891 191 91 95<br/>
	        				<b>Fax:</b> +1 123 45678910<br/>
	        				<b>Email:</b> <a href="#">surxonsohili@gmail.com</a>
	        			</p>
		    		</div>				
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Reklama</h3>
						<p>Dashnobod anorlari yana qayta yetishtirila boshlandi Ular o'z dovrig'ini yana qayta qozonadi. Bu yo'lda ishlar boshlab yuborildi.</p>
		    			<div>
		    				<img src="/assets/img/icons/facebook.png" width="32" alt="Facebook">
		    				<img src="/assets/img/icons/twitter.png" width="32" alt="Twitter">
		    				<img src="/assets/img/icons/linkedin.png" width="32" alt="LinkedIn">
							<img src="/assets/img/icons/rss.png" width="32" alt="RSS Feed">
						</div>
		    		</div>
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Fermerlar haqida</h3>
		    				<p>Agar siz ham fermer bo'lishini xoxlasangiz va sizda innovatsion g'oyalar bo'lsa o'zingiz yashab turgan tuman hokimligiga uchrashing va ro'yxatdan o'ting. Murojat uchun telefon +99891 191 91 95</p>
		    		</div>

		    	</div>
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="footer-copyright">&copy; 2019 <a href="#">Uzfarm.uz</a> Xizmatlar litsenziyalangan.</div>
		    		</div>
		    	</div>
		    </div>
	    </div>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/assets/js/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="/assets/js/bootstrap.min.js"></script>
		
		<!-- Scrolling Nav JavaScript -->
		<script src="/assets/js/jquery.easing.min.js"></script>
		<script src="/assets/js/scrolling-nav.js"></script>

    </body>
</html>